FROM golang:1.12-alpine AS builder
WORKDIR /app
COPY . ./
RUN apk add git g++
RUN go build -o main

FROM alpine
ENV DB_DIR="/app/db"
WORKDIR /app
RUN mkdir $DB_DIR
COPY --from=builder /app/main .
COPY ./utils /utils
VOLUME /app/db
RUN apk add ca-certificates python3 python3-dev nodejs && pip3 install --upgrade pip setuptools cfscrape
CMD ["./main"]
