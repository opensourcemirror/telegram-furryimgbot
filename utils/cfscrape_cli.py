import sys
import json

import cfscrape

def main():
    url = sys.argv[1]
    cookies, user_agent = cfscrape.get_tokens(url)

    print(json.dumps({
        'cookies': cookies,
        'user_agent': user_agent,
    }))

def help_message():
    print('Provide a URL to load, get a JSON dict of data.')

if __name__ == '__main__':
    if len(sys.argv) != 2:
        help_message()
    else:
        main()
