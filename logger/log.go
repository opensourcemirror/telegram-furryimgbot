package logger

import (
	"github.com/sirupsen/logrus"
)

// Log is the application logger.
var Log = logrus.New()
