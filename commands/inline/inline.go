package command

import (
	"strings"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"

	"huefox.com/syfaro/telegram-furryimgbot/logger"
	"huefox.com/syfaro/telegram-furryimgbot/sites"
)

func init() {
	finch.SetInline(&inline{})
}

type inline struct {
}

func (i inline) Execute(f *finch.Finch, query tgbotapi.InlineQuery) error {
	var results []interface{}
	var err error

	for _, site := range sites.KnownSites {
		queryStr := strings.Trim(query.Query, " ")
		if site.IsSupportedURL(queryStr) {
			posts, e := site.GetImageURLs(queryStr, *query.From)
			err = e
			if posts != nil && len(posts) != 0 {
				for _, post := range posts {
					results = append(results, post.BuildInlineResponse()...)
				}
			}
			break
		}
	}

	fieldLogger := logger.Log.WithFields(logrus.Fields{
		"id":    query.ID,
		"query": query.Query,
	})

	if err != nil {
		results = []interface{}{tgbotapi.NewInlineQueryResultArticle("unknown", "Unknown link", err.Error())}
		fieldLogger.Debug("Unknown link")
	}

	if results == nil {
		results = []interface{}{tgbotapi.NewInlineQueryResultArticle("unknown", "Bad link", "Unable to get data")}
		fieldLogger.Debug("Unable to load from link")
	}

	cacheTime := 100
	if f.API.Debug {
		cacheTime = 0
	}

	config := tgbotapi.InlineConfig{
		InlineQueryID: query.ID,
		IsPersonal:    false,
		CacheTime:     cacheTime,
		Results:       results,
	}

	fieldLogger.Info("Responding to inline query")

	_, err = f.API.Request(config)
	return err
}
