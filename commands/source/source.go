package command

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const imageSearchEndpoint = "https://fa.huefox.com/api/v1/image"
const imageDistanceThreshold = 5

func init() {
	finch.RegisterCommand(&sourceCommand{})
}

type imageSearchResult struct {
	ID       int    `json:"id"`
	Distance int    `json:"distance"`
	URL      string `json:"url"`
	FileName string `json:"filename"`
}

type sourceCommand struct {
	finch.CommandBase
}

func (sourceCommand) Help() finch.Help {
	return finch.Help{
		Name: "Source",
	}
}

func (sourceCommand) ShouldExecute(message tgbotapi.Message) bool {
	return message.Chat.IsPrivate() && message.Photo != nil && len(message.Photo) > 0
}

func (cmd sourceCommand) Execute(message tgbotapi.Message) error {
	_, err := cmd.API.Request(tgbotapi.NewChatAction(message.Chat.ID, tgbotapi.ChatTyping))
	if err != nil {
		return err
	}

	var biggestID string
	mostPixels := 0

	for _, photo := range message.Photo {
		pixels := photo.Height * photo.Width

		if pixels > mostPixels {
			biggestID = photo.FileID
			mostPixels = pixels
		}
	}

	fileURL, err := cmd.API.GetFileDirectURL(biggestID)
	if err != nil {
		return err
	}

	resp, err := http.Get(fileURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	fw, err := w.CreateFormFile("image", "file")
	if err != nil {
		return err
	}

	io.Copy(fw, resp.Body)
	w.Close()

	req, err := http.NewRequest("POST", imageSearchEndpoint, &b)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", w.FormDataContentType())
	req.Header.Set("X-Api-Key", cmd.Config.Get("fautil_key").(string))

	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	d := json.NewDecoder(resp.Body)

	var results []imageSearchResult
	if err = d.Decode(&results); err != nil {
		return err
	}

	if len(results) == 0 || results[0].Distance > imageDistanceThreshold {
		return cmd.QuickReply(message, "Sorry, I couldn't find this image on FurAffinity.")
	}

	return cmd.QuickReply(message, fmt.Sprintf("https://www.furaffinity.net/view/%d/", results[0].ID))
}
