package command

import (
	"encoding/json"
	"strconv"
	"strings"
	"sync"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"huefox.com/syfaro/telegram-furryimgbot/data"

	"github.com/boltdb/bolt"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	twitterOAuth1 "github.com/dghubble/oauth1/twitter"
)

type twitterAuthCommand struct {
	finch.CommandBase

	oauthConfig *oauth1.Config

	credentials     map[int][]string
	credentialsSync *sync.Mutex
}

func (cmd *twitterAuthCommand) Init(c *finch.CommandState, f *finch.Finch) error {
	cmd.CommandState = c
	cmd.Finch = f

	if _, ok := f.Config.Get("twitter_consumer_key").(string); !ok {
		return nil
	}

	config := &oauth1.Config{
		ConsumerKey:    f.Config.Get("twitter_consumer_key").(string),
		ConsumerSecret: f.Config.Get("twitter_secret_key").(string),
		CallbackURL:    "oob",
		Endpoint:       twitterOAuth1.AuthorizeEndpoint,
	}

	cmd.oauthConfig = config

	cmd.credentials = make(map[int][]string)
	cmd.credentialsSync = &sync.Mutex{}

	return nil
}

func (twitterAuthCommand) Help() finch.Help {
	return finch.Help{
		Name:        "Twitter Auth",
		Description: "Authorize with your Twitter account to get locked posts",
		Botfather: [][]string{
			{"twitter", "Authorize your Twitter account"},
		},
	}
}

func (twitterAuthCommand) ShouldExecute(message tgbotapi.Message) bool {
	return finch.SimpleCommand("twitter", message.Text)
}

func (cmd *twitterAuthCommand) Execute(message tgbotapi.Message) error {
	if !message.Chat.IsPrivate() {
		return cmd.QuickReply(message, "You may only use this command in a private chat")
	}

	token, secret, err := cmd.oauthConfig.RequestToken()
	if err != nil {
		return err
	}

	cmd.credentialsSync.Lock()
	cmd.credentials[message.From.ID] = []string{token, secret}
	cmd.credentialsSync.Unlock()

	cmd.SetWaiting(message.From.ID)

	authURL, err := cmd.oauthConfig.AuthorizationURL(token)
	if err != nil {
		return err
	}

	b := &strings.Builder{}

	b.WriteString("Please visit the following URL to authorize your Twitter account, then tell me the returned code.\n\n")
	b.WriteString(authURL.String())

	msg := tgbotapi.NewMessage(message.Chat.ID, b.String())
	msg.ReplyMarkup = tgbotapi.ForceReply{ForceReply: true, Selective: true}

	cmd.SendMessage(msg)

	return nil
}

func (cmd *twitterAuthCommand) ExecuteWaiting(message tgbotapi.Message) error {
	cmd.ReleaseWaiting(message.From.ID)

	code := strings.Trim(message.Text, " ")

	cmd.credentialsSync.Lock()
	creds := cmd.credentials[message.From.ID]
	cmd.credentialsSync.Unlock()

	access, secret, err := cmd.oauthConfig.AccessToken(creds[0], creds[1], code)
	if err != nil {
		return err
	}

	err = data.DB.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("twittercreds"))
		if err != nil {
			return err
		}

		bytes, err := json.Marshal(data.UserCred{
			AccessToken: access,
			SecretToken: secret,
		})
		if err != nil {
			return err
		}

		return b.Put([]byte(strconv.Itoa(message.From.ID)), bytes)
	})
	if err != nil {
		return err
	}

	token := oauth1.NewToken(access, secret)

	client := twitter.NewClient(cmd.oauthConfig.Client(oauth1.NoContext, token))

	user, _, err := client.Accounts.VerifyCredentials(nil)
	if err != nil {
		return err
	}

	return cmd.QuickReply(message, "Thank you, @"+user.ScreenName)
}

func init() {
	finch.RegisterCommand(&twitterAuthCommand{})
}
