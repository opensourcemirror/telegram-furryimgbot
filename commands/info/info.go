package command

import (
	"sort"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"huefox.com/syfaro/telegram-furryimgbot/sites"
)

func init() {
	finch.RegisterCommand(&startCommand{})
	finch.RegisterCommand(&helpCommand{})
}

type startCommand struct {
	finch.CommandBase
}

func (startCommand) Help() finch.Help {
	return finch.Help{
		Name: "Start",
	}
}

func (startCommand) ShouldExecute(message tgbotapi.Message) bool {
	return finch.SimpleCommand("start", message.Text)
}

func (cmd startCommand) Execute(message tgbotapi.Message) error {
	return cmd.QuickReply(message, "Use me as an inline bot to mirror images from FurAffinity, e621, and more. Send me a photo and I'll try to find the source on FurAffinity. Ask me for /help@@ to see a full list of sites.")
}

type helpCommand struct {
	finch.CommandBase
}

func (helpCommand) Help() finch.Help {
	return finch.Help{
		Name: "Help",
	}
}

func getSiteNames() []string {
	names := make([]string, len(sites.KnownSites))
	for idx, val := range sites.KnownSites {
		names[idx] = val.Name()
	}
	// direct links is always assumed to be last in knownsites
	// as this is the most generic, we want it last in this list
	sort.Strings(names[:len(names)-1])
	return names
}

func (helpCommand) ShouldExecute(message tgbotapi.Message) bool {
	return finch.SimpleCommand("help", message.Text)
}

func (cmd helpCommand) Execute(message tgbotapi.Message) error {
	msg := "Use me as an inline bot to mirror images from various sites.\n\nWe support:\n"
	for _, site := range getSiteNames() {
		msg += "· " + site + "\n"
	}
	msg += "\nYou can also authorize your Twitter account to get images from a locked account with /twitter@@."
	msg += "\n\nIf you send me a photo, I'll try to find its source on FurAffinity (only images from the past few weeks right now though)."
	msg += "\n\nSend @Syfaro a message if you have issues or suggestions."
	return cmd.QuickReply(message, msg)
}
