package command

import (
	"mvdan.cc/xurls"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"huefox.com/syfaro/telegram-furryimgbot/sites"
)

var urlFinder = xurls.Strict()

func init() {
	finch.RegisterCommand(&messageCommand{})
}

type messageCommand struct {
	finch.CommandBase
}

func (cmd messageCommand) ShouldExecute(message tgbotapi.Message) bool {
	for _, site := range sites.KnownSites {
		for _, url := range urlFinder.FindAllString(message.Text, -1) {
			if site.IsSupportedURL(url) {
				return true
			}
		}

		if message.ReplyToMessage != nil {
			return messageCommand.ShouldExecute(cmd, *message.ReplyToMessage)
		}
	}

	return false
}

func (messageCommand) Help() finch.Help {
	return finch.Help{
		Name: "Message",
	}
}

func (cmd messageCommand) processURL(site sites.Site, message tgbotapi.Message, url string) error {
	posts, err := site.GetImageURLs(url, *message.From)
	if err != nil {
		return err
	}

	if posts == nil || len(posts) == 0 {
		return nil
	}

	var msg tgbotapi.Chattable

	if len(posts) == 1 {
		photo := tgbotapi.NewPhotoShare(message.Chat.ID, posts[0].URL)
		photo.ReplyToMessageID = message.MessageID
		photo.Caption = posts[0].Caption

		msg = photo
	} else {
		var medias []interface{}

		for _, post := range posts {
			item := tgbotapi.NewInputMediaPhoto(post.URL)
			item.Caption = post.Caption

			medias = append(medias, item)
		}

		photo := tgbotapi.NewMediaGroup(message.Chat.ID, medias)
		photo.ReplyToMessageID = message.MessageID

		msg = photo
	}

	_, err = cmd.Finch.API.Send(msg)
	return err
}

func (cmd messageCommand) Execute(message tgbotapi.Message) error {
	for _, site := range sites.KnownSites {
		for _, url := range urlFinder.FindAllString(message.Text, -1) {
			if site.IsSupportedURL(url) {
				cmd.processURL(site, message, url)
			}
		}

		if message.ReplyToMessage != nil {
			for _, url := range urlFinder.FindAllString(message.ReplyToMessage.Text, -1) {
				if site.IsSupportedURL(url) {
					cmd.processURL(site, *message.ReplyToMessage, url)
				}
			}
		}
	}

	return nil
}
