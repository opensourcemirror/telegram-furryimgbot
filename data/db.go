package data

import (
	"os"
	"path"

	"github.com/boltdb/bolt"
)

// DB is a database for storing information.
var DB *bolt.DB

// UserCred is user credentials for Twitter.
type UserCred struct {
	AccessToken string `json:"access"`
	SecretToken string `json:"secret"`
}

var dbFolder = os.Getenv("DB_DIR")

func ensureFolderExists() {
	_, err := os.Stat(dbFolder)
	if os.IsNotExist(err) {
		os.Mkdir(dbFolder, 0600)
	}
}

func init() {
	ensureFolderExists()

	d, err := bolt.Open(path.Join(dbFolder, "app.db"), 0600, nil)
	if err != nil {
		panic(err)
	}

	DB = d
}
