package main

import (
	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"

	"huefox.com/syfaro/telegram-furryimgbot/logger"
	"huefox.com/syfaro/telegram-furryimgbot/sites"

	_ "huefox.com/syfaro/telegram-furryimgbot/commands/info"
	_ "huefox.com/syfaro/telegram-furryimgbot/commands/inline"
	_ "huefox.com/syfaro/telegram-furryimgbot/commands/message"
	_ "huefox.com/syfaro/telegram-furryimgbot/commands/source"
)

func main() {
	f := finch.NewFinch("")

	switch v := f.Config.Get("debug").(type) {
	case bool:
		f.API.Debug = v
	case string:
		f.API.Debug = v == "true"
	default:
		f.API.Debug = false
	}

	if f.API.Debug {
		logger.Log.SetLevel(logrus.DebugLevel)
	} else {
		logger.Log.SetLevel(logrus.InfoLevel)
	}

	tgbotapi.SetLogger(logger.Log)

	sites.ConfigureSites(f)

	f.Start()
}
