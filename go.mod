module huefox.com/syfaro/telegram-furryimgbot

require (
	cloud.google.com/go v0.37.4 // indirect
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/Syfaro/finch v1.0.1-0.20190429050912-8d5a35e62eac
	github.com/boltdb/bolt v1.3.1
	github.com/cenkalti/backoff v2.1.1+incompatible // indirect
	github.com/certifi/gocertifi v0.0.0-20190415143156-92f724a62f3e // indirect
	github.com/dghubble/go-twitter v0.0.0-20190305084156-0022a70e9bee
	github.com/dghubble/oauth1 v0.5.0
	github.com/dghubble/sling v1.2.0 // indirect
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.0.0-rc1
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/google/btree v1.0.0 // indirect
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/google/pprof v0.0.0-20190404155422-f8f10df84213 // indirect
	github.com/hashicorp/golang-lru v0.5.1 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/sirupsen/logrus v1.4.1
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190426145343-a29dc8fdc734 // indirect
	golang.org/x/exp v0.0.0-20190426190305-956cc1757749 // indirect
	golang.org/x/image v0.0.0-20190424155947-59b11bec70c7 // indirect
	golang.org/x/lint v0.0.0-20190409202823-959b441ac422 // indirect
	golang.org/x/mobile v0.0.0-20190415191353-3e0bab5405d6 // indirect
	golang.org/x/net v0.0.0-20190424112056-4829fb13d2c6 // indirect
	golang.org/x/oauth2 v0.0.0-20190402181905-9f3314589c9a
	golang.org/x/sys v0.0.0-20190428183149-804c0c7841b5 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	golang.org/x/tools v0.0.0-20190428024724-550556f78a90 // indirect
	google.golang.org/api v0.4.0 // indirect
	google.golang.org/appengine v1.5.0 // indirect
	google.golang.org/genproto v0.0.0-20190425155659-357c62f0e4bb // indirect
	google.golang.org/grpc v1.20.1 // indirect
	honnef.co/go/tools v0.0.0-20190418001031-e561f6794a2a // indirect
	huefox.com/syfaro/go-e621 v1.0.0
	mvdan.cc/xurls v1.1.1-0.20180901190342-70405f5eab51
)

go 1.13
