package sites

import (
	"encoding/json"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/Syfaro/finch"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"huefox.com/syfaro/telegram-furryimgbot/logger"
)

const imageURLEndpoint = "https://fa.huefox.com/api/v1/url"

type imageURLResult struct {
	ID       int    `json:"id"`
	URL      string `json:"url"`
	FileName string `json:"filename"`
}

type fa struct {
	cookies   []*http.Cookie
	fautilKey string
	client    *http.Client
}

func (fa) Name() string {
	return "FurAffinity"
}

func (f *fa) loadConfig(finch *finch.Finch) {
	if _, ok := finch.Config.Get("fa_a").(string); !ok {
		return
	}

	cookies := []*http.Cookie{
		{Name: "b", Value: finch.Config.Get("fa_b").(string)},
		{Name: "a", Value: finch.Config.Get("fa_a").(string)},
	}

	f.cookies = cookies
	f.fautilKey = finch.Config.Get("fautil_key").(string)
}

func (fa) IsSupportedURL(url string) bool {
	return strings.Contains(url, "furaffinity.net/view/") || strings.Contains(url, "furaffinity.net/full/") || strings.Contains(url, "facdn.net/art/")
}

type cfscrape struct {
	Cookies   map[string]string `json:"cookies"`
	UserAgent string            `json:"user_agent"`
}

func (f *fa) initClient() {
	if f.client != nil {
		return
	}

	timeout := time.Duration(4 * time.Second)
	client := &http.Client{
		Timeout: timeout,
	}
	f.client = client
}

func (f fa) handleDirectLink(postURL string) ([]PostInfo, error) {
	logger.Log.Debug("Attempting to load FA direct link")

	post := strings.TrimPrefix(postURL, "https://")
	post = strings.TrimPrefix(post, "http://")
	post = "https://" + post

	logger.Log.Debugf("Converted post link is %s", post)

	v := url.Values{}
	v.Add("url", post)

	u, _ := url.Parse(imageURLEndpoint)
	u.RawQuery = v.Encode()

	req, _ := http.NewRequest("GET", u.String(), nil)
	req.Header.Set("X-Api-Key", f.fautilKey)

	f.initClient()

	resp, err := f.client.Do(req)
	if err != nil {
		logger.Log.Warnf("Error loading, falling back to direct loading")
		d := direct{}
		return d.GetImageURLs(postURL, tgbotapi.User{})
	}
	defer resp.Body.Close()

	var results []imageURLResult
	d := json.NewDecoder(resp.Body)
	if err = d.Decode(&results); err != nil {
		logger.Log.Warnf("Unable to decode results")
		return nil, err
	}

	if len(results) == 0 {
		logger.Log.Debug("No results, falling back to direct loading")
		d := direct{}
		return d.GetImageURLs(postURL, tgbotapi.User{})
	}

	logger.Log.Debugf("Got %d results, returning first: %d", len(results), results[0].ID)
	return f.GetImageURLs("https://www.furaffinity.net/view/"+strconv.Itoa(results[0].ID), tgbotapi.User{})
}

func (f fa) GetImageURLs(postURL string, _ tgbotapi.User) ([]PostInfo, error) {
	if strings.Contains(postURL, "facdn.net/art/") {
		return f.handleDirectLink(postURL)
	}

	u, err := url.Parse(postURL)
	if err != nil {
		return nil, err
	}

	jar, _ := cookiejar.New(nil)
	jar.SetCookies(u, f.cookies)

	client := &http.Client{Jar: jar}
	resp, err := client.Get(u.String())
	if err != nil {
		return nil, err
	}

	logger.Log.Debugf("Got status code %d\n", resp.StatusCode)

	if resp.StatusCode == 503 || resp.StatusCode == 429 {
		out, err := exec.Command("python3", "/utils/cfscrape_cli.py", "https://www.furaffinity.net").Output()
		if err != nil {
			logger.Log.Warnf("Unable to run cfscrape: %s\n", err.Error())
			return nil, err
		}

		var cf cfscrape
		err = json.Unmarshal(out, &cf)
		if err != nil {
			return nil, err
		}

		logger.Log.Printf("Got output: %v\n", cf)

		cookies := f.cookies

		for name, value := range cf.Cookies {
			cookies = append(cookies, &http.Cookie{
				Name:  name,
				Value: value,
			})
		}

		jar.SetCookies(u, cookies)

		client := &http.Client{Jar: jar}

		req, _ := http.NewRequest("GET", u.String(), nil)
		req.Header.Set("User-Agent", cf.UserAgent)

		r, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		resp = r

		logger.Log.Debugf("Got new status code %d\n", resp.StatusCode)
	}

	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		return nil, err
	}

	src, _ := doc.Find("#submissionImg").First().Attr("src")
	image := "https:" + src

	return []PostInfo{{
		FileType: getFileExt(image),
		URL:      image,
		Thumb:    image,
		Caption:  u.String(),
	}}, nil
}
