package sites

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var weasylExtractor = regexp.MustCompile(`https?:\/\/www\.weasyl\.com\/(?:(?:~|%7)(?:\w+)\/submissions|submission)\/(?P<id>\d+)(?:\/\S+)`)
var weasylEndpoint = "https://www.weasyl.com/api/submissions/%s/view"

type weasyl struct {
	client *http.Client
	token  string
}

func (weasyl) Name() string {
	return "Weasyl"
}

func (w *weasyl) loadConfig(f *finch.Finch) {
	w.client = &http.Client{}

	if token, ok := f.Config.Get("weasyl_token").(string); ok {
		w.token = token
	}
}

type weasylSubmissionItem struct {
	Rating      string           `json:"rating"`
	Embedlink   interface{}      `json:"embedlink"`
	Description string           `json:"description"`
	Title       string           `json:"title"`
	Media       weasylMedia      `json:"media"`
	PostedAt    string           `json:"posted_at"`
	Tags        []string         `json:"tags"`
	Comments    int64            `json:"comments"`
	OwnerMedia  weasylOwnerMedia `json:"owner_media"`
	Subtype     string           `json:"subtype"`
	Favorited   bool             `json:"favorited"`
	Link        string           `json:"link"`
	FolderName  string           `json:"folder_name"`
	Favorites   int64            `json:"favorites"`
	Type        string           `json:"type"`
	Owner       string           `json:"owner"`
	Folderid    int64            `json:"folderid"`
	OwnerLogin  string           `json:"owner_login"`
	Views       int64            `json:"views"`
	FriendsOnly bool             `json:"friends_only"`
	Submitid    int64            `json:"submitid"`
}

type weasylMedia struct {
	ThumbnailGenerated []weasylCover      `json:"thumbnail-generated"`
	Cover              []weasylCover      `json:"cover"`
	Thumbnail          []weasylCover      `json:"thumbnail"`
	Submission         []weasylSubmission `json:"submission"`
}

type weasylCover struct {
	URL     string `json:"url"`
	Mediaid int64  `json:"mediaid"`
}

type weasylSubmission struct {
	URL     string      `json:"url"`
	Mediaid int64       `json:"mediaid"`
	Links   weasylLinks `json:"links"`
}

type weasylLinks struct {
	Cover []weasylCover `json:"cover"`
}

type weasylOwnerMedia struct {
	Banner []weasylCover `json:"banner"`
	Avatar []weasylCover `json:"avatar"`
}

func (weasyl) IsSupportedURL(url string) bool {
	return weasylExtractor.FindStringSubmatch(url) != nil
}

func (w weasyl) GetImageURLs(url string, from tgbotapi.User) ([]PostInfo, error) {
	match := weasylExtractor.FindStringSubmatch(url)
	id := match[1]

	endpoint := fmt.Sprintf(weasylEndpoint, id)

	req, _ := http.NewRequest("GET", endpoint, nil)

	if w.token != "" {
		req.Header.Set("X-Weasyl-API-Key", w.token)
	}

	resp, err := w.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var sub weasylSubmissionItem

	decoder := json.NewDecoder(resp.Body)
	if err = decoder.Decode(&sub); err != nil {
		return nil, err
	}

	var posts []PostInfo

	for idx, post := range sub.Media.Submission {
		thumb := sub.Media.Thumbnail[idx]

		posts = append(posts, PostInfo{
			FileType: getFileExt(post.URL),
			URL:      post.URL,
			FullURL:  post.URL,
			Thumb:    thumb.URL,
			Caption:  url,
		})
	}

	return posts, nil
}
