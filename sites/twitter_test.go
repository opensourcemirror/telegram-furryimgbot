package sites

import (
	"testing"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/stretchr/testify/assert"
)

var testStr = "testing something \n\nhttps://t.co/7Prvj7hYPd\n\npls ignore \n\nhttps://t.co/4myFJksuKR\n\n:3 https://t.co/YERKTUlNOZ"

var newStr = "testing something \n\nhttps://syfaro.net/\n\npls ignore \n\nhttps://www.furaffinity.net/user/syfaro\n\n:3 https://t.co/YERKTUlNOZ"

func TestHumanifyURLs(t *testing.T) {
	str := humanifyURLs(testStr, []twitter.URLEntity{
		{
			Indices:     twitter.Indices{20, 43},
			DisplayURL:  "syfaro.net",
			ExpandedURL: "https://syfaro.net/",
			URL:         "https://t.co/7Prvj7hYPd",
		},
		{
			Indices:     twitter.Indices{58, 81},
			DisplayURL:  "furaffinity.net/user/syfaro",
			ExpandedURL: "https://www.furaffinity.net/user/syfaro",
			URL:         "https://t.co/4myFJksuKR",
		},
	})

	assert.Equal(t, newStr, str)
}
