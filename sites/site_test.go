package sites

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetKeyboard(t *testing.T) {
	board := getKeyboard("source", "direct")

	assert.Len(t, board.InlineKeyboard, 1)
	assert.Len(t, board.InlineKeyboard[0], 2)

	assert.Equal(t, "Direct Link", board.InlineKeyboard[0][0].Text)
	assert.Equal(t, "direct", *board.InlineKeyboard[0][0].URL)

	assert.Equal(t, "Source", board.InlineKeyboard[0][1].Text)
	assert.Equal(t, "source", *board.InlineKeyboard[0][1].URL)

	board = getKeyboard("direct", "direct")

	assert.Len(t, board.InlineKeyboard, 1)
	assert.Len(t, board.InlineKeyboard[0], 1)

	assert.Equal(t, "Direct Link", board.InlineKeyboard[0][0].Text)
	assert.Equal(t, "direct", *board.InlineKeyboard[0][0].URL)
}

func TestGetFileExt(t *testing.T) {
	ext := getFileExt("test.jpg")
	assert.Equal(t, "jpg", ext)

	ext = getFileExt("test")
	assert.Equal(t, "", ext)
}
