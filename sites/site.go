package sites

import (
	"strings"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/gofrs/uuid"
	"huefox.com/syfaro/telegram-furryimgbot/logger"
)

// Site is a supported site to load an image from.
type Site interface {
	Name() string
	IsSupportedURL(url string) bool
	GetImageURLs(url string, from tgbotapi.User) ([]PostInfo, error)
	loadConfig(*finch.Finch)
}

// PostInfo is a collection of information about a post.
type PostInfo struct {
	FileType string
	URL      string
	FullURL  string
	Thumb    string
	Caption  string
	Message  string
}

func getKeyboard(sourceURL, imageURL string) *tgbotapi.InlineKeyboardMarkup {
	var buttons = []tgbotapi.InlineKeyboardButton{
		{Text: "Direct Link", URL: &imageURL},
	}

	if sourceURL != imageURL {
		buttons = append(buttons, tgbotapi.InlineKeyboardButton{Text: "Source", URL: &sourceURL})
	}

	return &tgbotapi.InlineKeyboardMarkup{
		InlineKeyboard: [][]tgbotapi.InlineKeyboardButton{buttons},
	}
}

func getFileExt(name string) string {
	idx := strings.LastIndex(name, ".")
	if idx == -1 {
		return ""
	}
	return name[idx+1:]
}

// BuildInlineResponse creates an inline response from a PostInfo struct.
func (post PostInfo) BuildInlineResponse() []interface{} {
	postID := genID()

	fullURL := post.FullURL
	if fullURL == "" {
		fullURL = post.URL
	}

	var results []interface{}

	switch post.FileType {
	case "gif":
		results = append(results, tgbotapi.InlineQueryResultGIF{
			Type:        "gif",
			ID:          postID,
			URL:         post.URL,
			ThumbURL:    post.Thumb,
			ReplyMarkup: getKeyboard(post.Caption, fullURL),
		})
	case "mp4":
		results = append(results, tgbotapi.InlineQueryResultVideo{
			Type:        "video",
			ID:          postID,
			URL:         post.URL,
			ThumbURL:    post.Thumb,
			ReplyMarkup: getKeyboard(post.Caption, fullURL),
			MimeType:    "video/mp4",
			Title:       "Twitter Video",
		})
	case "webm", "swf":
		results = append(results, tgbotapi.NewInlineQueryResultArticle("unknown", "Unsupported Type", "unsupported"))
	default:
		results = append(results, tgbotapi.InlineQueryResultPhoto{
			Type:        "photo",
			ID:          postID,
			URL:         post.URL,
			ThumbURL:    post.Thumb,
			ReplyMarkup: getKeyboard(post.Caption, fullURL),
		})
	}

	if _, ok := results[0].(tgbotapi.InlineQueryResultArticle); ok {
		logger.Log.Debug("Result was article, skipping message check")
		return results
	}

	if post.Message == "" {
		logger.Log.Debug("Photo did not have extra message")
		return results
	}

	logger.Log.Debug("Adding message to photo")

	if messageItem, ok := results[0].(tgbotapi.InlineQueryResultPhoto); ok {
		messageItem.ID = genID()
		messageItem.Caption = post.Message

		results = append(results, messageItem)
	}

	return results
}

// KnownSites are the sites we should operate on.
var KnownSites = []Site{
	&e621{},
	&fa{},
	&twit{},
	&mastodon{},
	&weasyl{},
	&direct{},
}

func genID() string {
	u, _ := uuid.NewV4()
	return u.String()
}

// ConfigureSites initializes the configuration for each site.
func ConfigureSites(f *finch.Finch) {
	for _, site := range KnownSites {
		site.loadConfig(f)
	}
}
