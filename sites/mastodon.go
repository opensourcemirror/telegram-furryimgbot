package sites

import (
	"encoding/json"
	"net/http"
	"net/url"
	"regexp"
	"sync"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type mastodon struct {
	instanceCacheSync *sync.RWMutex
	instanceCache     map[string]bool
}

func (mastodon) Name() string {
	return "Mastodon"
}

func (m *mastodon) loadConfig(*finch.Finch) {
	m.instanceCache = make(map[string]bool)
	m.instanceCacheSync = &sync.RWMutex{}
}

func (mastodon) checkIfMasto(host string) bool {
	resp, err := http.Head(host + "/api/v1/instance")
	if err != nil {
		return false
	}
	resp.Body.Close()
	if resp.StatusCode != 200 {
		return false
	}
	return true
}

func (mastodon) getSchemeHost(u string) string {
	ur, _ := url.Parse(u)
	return ur.Scheme + "://" + ur.Host
}

func (m *mastodon) IsSupportedURL(u string) bool {
	host := m.getSchemeHost(u)
	m.instanceCacheSync.RLock()
	isMasto, ok := m.instanceCache[host]
	m.instanceCacheSync.RUnlock()
	if ok {
		return isMasto
	}
	isMasto = m.checkIfMasto(host)
	m.instanceCacheSync.Lock()
	m.instanceCache[host] = isMasto
	m.instanceCacheSync.Unlock()
	return isMasto
}

var mastodonExtractor = regexp.MustCompile(`(?P<host>https?:\/\/(?:\S+))\/(?:notice|users\/\w+\/statuses|@\w+)\/(?P<id>\d+)`)

type mastodonUsefulInfo struct {
	URL              string `json:"url"`
	MediaAttachments []struct {
		URL        string `json:"url"`
		PreviewURL string `json:"preview_url"`
	} `json:"media_attachments"`
}

func (m mastodon) GetImageURLs(postURL string, _ tgbotapi.User) ([]PostInfo, error) {
	match := mastodonExtractor.FindStringSubmatch(postURL)
	if match == nil {
		return nil, nil
	}

	host := match[1]
	statusID := match[2]

	resp, err := http.Get(host + "/api/v1/statuses/" + statusID)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var data mastodonUsefulInfo

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&data)
	if err != nil {
		return nil, err
	}

	if len(data.MediaAttachments) == 0 {
		return nil, nil
	}

	var postInfos []PostInfo

	for _, media := range data.MediaAttachments {
		postInfos = append(postInfos, PostInfo{
			FileType: getFileExt(media.URL),
			URL:      media.URL,
			Thumb:    media.PreviewURL,
			Caption:  data.URL,
		})
	}

	return postInfos, nil
}
