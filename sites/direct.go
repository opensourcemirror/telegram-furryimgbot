package sites

import (
	"net/http"
	"strings"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"

	"huefox.com/syfaro/telegram-furryimgbot/logger"
)

var extensions = []string{"png", "jpg", "jpeg", "gif"}
var contentTypes = []string{"image/png", "image/jpeg", "image/gif"}

type direct struct {
}

func (direct) Name() string {
	return "direct links"
}

func (direct) loadConfig(*finch.Finch) {}

func (direct) IsSupportedURL(url string) bool {
	withFields := logger.Log.WithFields(logrus.Fields{
		"url": url,
	})

	withFields.Debug("Evaluating if URL is valid")

	lower := strings.ToLower(url)

	hasExt := false

	for _, ext := range extensions {
		if strings.HasSuffix(lower, ext) {
			hasExt = true
			break
		}
	}

	if !hasExt {
		withFields.Debug("Did not have valid extension")
		return false
	}

	resp, err := http.Head(url)
	if err != nil {
		withFields.WithError(err).Info("Unable to make HEAD request to image")
		return false
	}

	respType := strings.ToLower(resp.Header.Get("Content-Type"))

	for _, ctype := range contentTypes {
		if respType == ctype {
			return true
		}
	}

	return false
}

func (d direct) GetImageURLs(postURL string, _ tgbotapi.User) ([]PostInfo, error) {
	return []PostInfo{{
		FileType: getFileExt(postURL),
		URL:      postURL,
		Thumb:    postURL,
		Caption:  postURL,
	}}, nil
}
