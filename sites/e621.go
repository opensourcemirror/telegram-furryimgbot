package sites

import (
	"strconv"
	"strings"

	"github.com/Syfaro/finch"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	goe621 "huefox.com/syfaro/go-e621"
)

type e621 struct{}

func (e621) Name() string {
	return "e621"
}

func (e621) loadConfig(*finch.Finch) {}

func (e621) IsSupportedURL(url string) bool {
	return goe621.ParsePostURL(url) != nil || goe621.ParseDirectURL(url) != nil
}

func (e e621) GetImageURLs(url string, _ tgbotapi.User) ([]PostInfo, error) {
	var post *goe621.Post
	var err error

	if postInfo := goe621.ParsePostURL(url); postInfo != nil {
		post, err = goe621.GetPost(postInfo.ID)
	} else if postInfo := goe621.ParseDirectURL(url); postInfo != nil {
		post, err = goe621.GetPostByMD5(postInfo.MD5)
	}

	if err != nil || post == nil {
		return nil, err
	}

	if post.FileURL == "/images/deleted-preview.png" {
		return nil, nil
	}

	var caption string
	if strings.Contains(url, "e621.net") {
		caption = "https://e621.net/post/show/" + strconv.Itoa(post.ID)
	} else {
		caption = "https://e926.net/post/show/" + strconv.Itoa(post.ID)
	}

	var fileURL string

	if (post.FileExt != "jpg" && post.FileExt != "gif") || post.FileSize >= 1000*1000*5 {
		fileURL = post.SampleURL
	} else {
		fileURL = post.FileURL
	}

	return []PostInfo{{
		FileType: post.FileExt,
		URL:      fileURL,
		FullURL:  post.FileURL,
		Thumb:    post.PreviewURL,
		Caption:  caption,
	}}, nil
}
