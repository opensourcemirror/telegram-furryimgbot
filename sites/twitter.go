package sites

import (
	"encoding/json"
	"regexp"
	"strconv"
	"strings"

	"github.com/Syfaro/finch"
	"github.com/boltdb/bolt"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"huefox.com/syfaro/telegram-furryimgbot/data"
	"huefox.com/syfaro/telegram-furryimgbot/logger"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	twitterOAuth1 "github.com/dghubble/oauth1/twitter"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
)

var twitMatch = regexp.MustCompile(`https?:\/\/twitter\.com\/(?P<username>\w+)\/status\/(?P<id>\d+)`)

type twit struct {
	consumerKey, secretKey string

	client *twitter.Client
}

func (twit) Name() string {
	return "Twitter"
}

func (t *twit) loadConfig(f *finch.Finch) {
	if _, ok := f.Config.Get("twitter_consumer_key").(string); !ok {
		return
	}

	t.consumerKey = f.Config.Get("twitter_consumer_key").(string)
	t.secretKey = f.Config.Get("twitter_secret_key").(string)

	config := &clientcredentials.Config{
		ClientID:     t.consumerKey,
		ClientSecret: t.secretKey,
		TokenURL:     "https://api.twitter.com/oauth2/token",
	}

	httpClient := config.Client(oauth2.NoContext)
	t.client = twitter.NewClient(httpClient)
}

func (twit) IsSupportedURL(url string) bool {
	return twitMatch.MatchString(url)
}

func toChar(i int) rune {
	return rune('A' + i)
}

// humanifyURLs is a horribly hacky way to replace all the Twitter URLs with
// the non-shortened version.
func humanifyURLs(text string, urls []twitter.URLEntity) string {
	newStr := text

	replacements := make(map[string]string)

	for idx, url := range urls {
		indices := url.Indices
		length := url.Indices[1] - url.Indices[0]

		letter := toChar(idx)
		replacement := strings.Repeat(string(letter), length)

		newStr = newStr[:url.Indices[0]] + replacement + newStr[indices[1]:]

		replacements[replacement] = url.ExpandedURL
	}

	for str, val := range replacements {
		newStr = strings.Replace(newStr, str, val, 1)
	}

	return newStr
}

func (t twit) GetImageURLs(postURL string, user tgbotapi.User) ([]PostInfo, error) {
	match := twitMatch.FindStringSubmatch(postURL)
	if match == nil {
		return nil, nil
	}

	tweetIDStr := match[2]
	tweetID, _ := strconv.ParseInt(tweetIDStr, 10, 64)

	var cred *data.UserCred

	err := data.DB.View(func(t *bolt.Tx) error {
		b := t.Bucket([]byte("twittercreds"))
		if b == nil {
			logger.Log.Debug("twitter bucket does not exist")
			return nil
		}

		bytes := b.Get([]byte(strconv.Itoa(user.ID)))
		if bytes == nil {
			logger.Log.WithField("user_id", user.ID).Debug("do not have credentials for user")
			return nil
		}

		err := json.Unmarshal(bytes, &cred)
		if err != nil {
			logger.Log.WithField("user_id", user.ID).Debug("unable to get saved credentials")
			return err
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	var status *twitter.Tweet

	if cred == nil {
		status, _, err = t.client.Statuses.Show(tweetID, &twitter.StatusShowParams{
			TweetMode: "extended",
		})
	} else {
		config := &oauth1.Config{
			ConsumerKey:    t.consumerKey,
			ConsumerSecret: t.secretKey,
			Endpoint:       twitterOAuth1.AuthorizeEndpoint,
		}
		token := oauth1.NewToken(cred.AccessToken, cred.SecretToken)
		client := twitter.NewClient(config.Client(oauth1.NoContext, token))

		logger.Log.Debug("Using saved Twitter credentials")

		status, _, err = client.Statuses.Show(tweetID, &twitter.StatusShowParams{
			TweetMode: "extended",
		})
	}

	if err != nil {
		return nil, err
	}

	if status == nil ||
		status.ExtendedEntities == nil ||
		status.ExtendedEntities.Media == nil ||
		len(status.ExtendedEntities.Media) == 0 {
		return nil, err
	}

	var postInfos []PostInfo

	for _, entity := range status.ExtendedEntities.Media {
		var t, realURL string

		if entity.Type == "video" {
			var idx, best int

			for i, variant := range entity.VideoInfo.Variants {
				if best == 0 || variant.Bitrate > best {
					idx = i
					best = variant.Bitrate
				}
			}

			variant := entity.VideoInfo.Variants[idx]
			t = "mp4"
			realURL = variant.URL
		} else {
			t = getFileExt(entity.MediaURLHttps)
			realURL = entity.MediaURLHttps
		}

		postInfos = append(postInfos, PostInfo{
			FileType: t,
			URL:      realURL,
			Thumb:    entity.MediaURLHttps,
			Caption:  entity.ExpandedURL,
			Message:  humanifyURLs(status.FullText, status.Entities.Urls),
		})
	}

	return postInfos, nil
}
