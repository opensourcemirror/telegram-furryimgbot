# telegram-furryimgbot

Mirror images from various furry websites into Telegram as an inline bot.

After being configured (optional, will only do SFW images from FurAffinity though), it will mirror images from URLs provided as inline queries.

## Configuration

Items either need to be set in config.json as lowercase or environment variables.

* `TOKEN` - Telegram Bot API token from Botfather
* `DEBUG` - If debugging information should be printed
* `FA_A` - FurAffinity cookie 'a'
* `FA_B` - FurAffinity cookie 'b'
* `TWITTER_CONSUMER_KEY` - Twitter application consumer access key
* `TWITTER_SECRET_KEY` - Twitter application consumer secret key
* `DB_DIR` - Folder to store app.db in, will be created if it does not exist. Docker defaults to `/app/db`.
* `FAUTIL_KEY` - API token for https://fa.huefox.com/

If using Twitter credentials from users, the `DB_DIR` folder must be persistent.

## Supported Sites

* FurAffinity
* e621
